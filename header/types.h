#ifndef TYPES_H
#define TYPES_H

typedef enum {
    COLOR_TYPE_CODE,
    COLOR_TYPE_RGB
} ColorType;

typedef union {
    int code;
    int rgb[3];
} ColorData;

typedef struct {
    ColorType type;
    ColorData data;
} Color;

#endif
