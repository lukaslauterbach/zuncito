#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <stdbool.h>

#include "types.h"

void exitWithError(const char *msg);
void resetStyle(void);
void setStyle(int n);
void setForeground(Color color);
void setBackground(Color color);
void setForeAndBackground(Color fg, Color bg);
bool matchesRGBHexColorString(const char *str, unsigned int strLen, int i, unsigned char *outRed, unsigned char *outGreen, unsigned char *outBlue);
int matchesInteger(const char* str, unsigned int strLen, int i, int maxDigits, int *outParsedInteger);
bool matchesString(const char *str, unsigned int strLen, int i, const char *searchStr, unsigned int searchStrLen);
bool matchesStringCaseInsensitive(const char *str, unsigned int strLen, int i, const char *searchStr, unsigned int searchStrLen);

#endif // FUNCTIONS_H
