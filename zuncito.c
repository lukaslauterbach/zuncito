#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <limits.h>
#include <ctype.h>

#include "header/types.h"
#include "header/functions.h"

// TODO:
//   - Ensure that the 'return i' behavior is uniform (e.g. that all either end on the last i inside the part or after it)
//   - Move app logic entry into a separate function to be able to call it directly from main or individually e.g. for e2e tests
//   - Clean string handling (maybe use a library or write own)
//   - Proper memory handling (of heap mem)

static char  *format              = NULL;
static int    formatLength        = 0;
static char **params              = NULL;
static int    paramCount          = 0;
static int    autoIndex           = 0;
static bool   usedPositionalIndex = false;

char getCharAt(int i, char *errorMsg) {
    assert(i >= 0);

    if (i >= formatLength) {
        exitWithError(errorMsg);
    }
    return format[i];
}

int processRGBComponent(int i, int *out) {
    int value = -1;
    int newI = matchesInteger(format, formatLength, i, 3, &value);

    if (newI == i) exitWithError("Expected positive integer with max. 3 digits after 'rgb('");
    if (value < 0 || value > 255) exitWithError("rgb value out of range (0-255)");

    *out = (unsigned char)value;
    return newI;
}

// takes index of first character inside color specifier (after '#')
// returns new 'i'. Returns the same i when it could not match a color specifier!
int processSingleColor(int i, Color *outColor) {
    assert(i >= 0);
    assert(outColor);

    // -- rgb() function
    // TODO: Allow whitespace between components! e.g. (255,0,0) vs ( 255 , 0 , 0 )
    const char *rgbPrefix = "rgb(";
    const int rgbPrefixLen = strlen(rgbPrefix);
    if (matchesStringCaseInsensitive(format, formatLength, i, rgbPrefix, rgbPrefixLen)) {
        i += rgbPrefixLen;

        for (int component = 0; component < 3; component++) {
            i = processRGBComponent(i, &(outColor->data.rgb[component]));
            char expected = component == 2 ? ')' : ',';
            if (getCharAt(i, "Expected ',' or ')' after rgb( specifier, got end of string") != expected) {
                exitWithError("Expected ',' or ')' after rgb( specifier, got something else (yes better message needed xD)");
            }
            i++; // get past ',' or ')'
        }
        outColor->type = COLOR_TYPE_RGB;
        return i;
    }

    // -- RRGGBB hex code
    unsigned char red, green, blue;
    bool isHexCode = matchesRGBHexColorString(format, formatLength, i, &red, &green, &blue);
    if (isHexCode) {
        outColor->type = COLOR_TYPE_RGB;
        outColor->data.rgb[0] = red;
        outColor->data.rgb[1] = green;
        outColor->data.rgb[2] = blue;
        return i + 6; // 6 because len(RRGGBB) == 6
    }

    // -- code
    int value = -1;
    int newI = matchesInteger(format, formatLength, i, 2, &value);
    //printf("value: %i\noldI: %i\nnewI: %i\n%c\n", value, i, newI, format[newI]);
    if (newI > i) {
        outColor->type = COLOR_TYPE_CODE;
        outColor->data.code = value;
        return newI;
    }

    // -- long names
    // NOTE: These must be in order corresponding to the color code (0-15)!
    // TODO: check if runtime lowercase/uppercase conversion is faster than to iterate through both
    const char *longNames[] = {
        "black", "red", "green", "yellow", "blue", "magenta", "cyan", "white",
        "BLACK", "RED", "GREEN", "YELLOW", "BLUE", "MAGENTA", "CYAN", "WHITE"
    };

    for (int colorCode = 0; colorCode < sizeof(longNames)/sizeof(longNames[0]); colorCode++) {
        if (matchesString(format, formatLength, i, longNames[colorCode], strlen(longNames[colorCode]))) {
            outColor->type = COLOR_TYPE_CODE;
            outColor->data.code = colorCode;
            return i + strlen(longNames[colorCode]);
        }
    }

    // -- short names
    // NOTE: These must be in order corresponding to the color code (0-15)!
    char c = getCharAt(i, "Expected color specifier, got end of string!");
    // TODO: check if runtime lowercase/uppercase conversion is faster than to iterate through both
    const char shortNames[] = {
        'k', 'r', 'g', 'y', 'b', 'm', 'c', 'w',
        'K', 'R', 'G', 'Y', 'B', 'M', 'C', 'W'
    };

    for (int colorCode = 0; colorCode < sizeof(shortNames)/sizeof(shortNames[0]); colorCode++) {
        if (c == shortNames[colorCode]) {
            outColor->type = COLOR_TYPE_CODE;
            outColor->data.code = colorCode;
            return i + 1; // + 1 because we advanced by one character
        }
    }

    return i;
}

// TODO: Currently, "{#/}" fails but the spec says it should do nothing (what to do here?)
// takes index of first character inside color specifier (after '#')
// returns the new 'i'
int processColor(int i) {
    assert(i >= 0);

    Color foreground;
    int foregroundI = processSingleColor(i, &foreground);
    bool gotForeground = foregroundI > i;
    i = foregroundI;

    char c = getCharAt(i, "Expected '/', '}' or other specifier, got end of string!");
    if (c == '/') {
        i++; // to get past '/'
        Color background;
        int backgroundI = processSingleColor(i, &background);
        bool gotBackground = backgroundI > i;
        i = backgroundI;

        if (gotForeground && gotBackground) {
            setForeAndBackground(foreground, background);
        } else if (gotBackground) {
            setBackground(background);
        } else {
            exitWithError("Invalid background color specifier!");
        }
    } else if (gotForeground) {
        setForeground(foreground);
    } else {
        exitWithError("Invalid foreground color specifier!");
    }

    return i;
}

// takes index of first character inside style specifier (after '!', "style" or ',' in multipart style specifiers)
// returns the new 'i'
int processSingleStyle(int i) {
#define STYLE_COUNT 9
#define ALTERNATIVES_COUNT 7

    const int styleCodes[STYLE_COUNT] = {
        1, 2, 3, 4, 5, 7, 8, 9
    };
    const char *styles[STYLE_COUNT][ALTERNATIVES_COUNT] = {
        { "s", "strong"     , "bold"       , NULL           , NULL    , NULL      , NULL }     ,
        { "d", "dim"        , "faint"      , NULL           , NULL    , NULL      , NULL }     ,
        { "i", "italic"     , NULL         , NULL           , NULL    , NULL      , NULL }     ,
        { "u", "underline"  , "underscored", NULL           , NULL    , NULL      , NULL }     ,
        { "b", "blink"      , "blinking"   , NULL           , NULL    , NULL      , NULL }     ,
        { "r", "reversed"   , "inverse"    , "inversed"     , "invert", "inverted", "reverse" },
        { "h", "hidden"     , "invisible"  , NULL           , NULL    , NULL      , NULL }     ,
        { "c", "crossed-out", "strike"     , "strikethrough", NULL    , NULL      , NULL }     ,
    };

    int longestMatchLength  = 0;
    int codeForLongestMatch = 0;
    for (int styleCode = 0; styleCode < STYLE_COUNT; styleCode++) {
        for (int alternative = 0; alternative < ALTERNATIVES_COUNT; alternative++) {
            const char* alt = styles[styleCode][alternative];
            if (!alt) break;

            // TODO: the strlen should be compile time or at least computed before the loop (maybe the compiler does it?)
            if (matchesStringCaseInsensitive(format, formatLength, i, alt, strlen(alt))) {
                if (strlen(alt) > longestMatchLength) {
                    longestMatchLength = strlen(alt);
                    codeForLongestMatch = styleCode;
                }
            }
        }
    }
    if (longestMatchLength > 0) {
        setStyle(styleCodes[codeForLongestMatch]);
        return i + longestMatchLength;
    }

    exitWithError("Invalid style specifier!");
    return -1; // Unreachable!
}

// takes index of first character inside style specifier (after '!' or "style")
// returns the new 'i'
int processStyle(int i) {
    while (true) {
        const char c = getCharAt(i, "Expected style specifier or comma, got end of string");
        if (c == ',') {
            i = processSingleStyle(i + 1);
        } else if (isspace(c) || c == '}') {
            break;
        } else {
            i = processSingleStyle(i);
        }
    }

    return i;
}

// takes index of fist character inside specifier (after '@')
// return the new 'i'
int processAllArgsSpecifier(int i, char **outStr) {
    assert(i >= 0);
    assert(i <= formatLength);

    char c = getCharAt(i, "Expected specifier, got end of string");
    if (c != '|') return i;
    *outStr = NULL;
    i++; // Go past starting '|'
    c = getCharAt(i, "Expected all args separator specifier, got end of string");

    // TODO: Allow longer than 100 character separators
    const int maxOutStrLength = 100;
    *outStr = (char*)malloc(maxOutStrLength + 1); // + 1 to ensure a '\0' at the end
    if (!(*outStr)) {
        exitWithError("Failed to malloc for all args separator specifier");
    }
    memset(*outStr, 0, maxOutStrLength + 1);

    // TODO: Factor this out into a 'eatUntil or captureStringUntil or whatever' (how to handle the escaping then?)
    int outStrIndex = 0;
    bool escaped = false; // to allow for '|\||'
    while (c != '|' || (c == '|' && escaped)) {
        if (c == '\\') {
            escaped = true;
            i++;
            c = getCharAt(i, "Expected all args separator specifier, got end of string");
            continue;
        }

        (*outStr)[outStrIndex] = c;

        i++;
        c = getCharAt(i, "Expected all args separator specifier, got end of string");

        outStrIndex++;
        if (outStrIndex >= maxOutStrLength) {
            exitWithError("All args separator has too many characters (max. 100)");
        }
        escaped = false;
    }

    return i + 1; // + 1 to get past ending '|'
}

// takes index of first character inside specifier (after '%')
// returns the new 'i'
int processSubstitution(int i, char **outAllArgsSeparator, int *outStartIndex, int *outEndIndex) {
    assert(i >= 0);

    char c = getCharAt(i, "Expected ' ' or '}' or index specifier, got end of string");

    if (c == ' ' || c == '}') {
        *outStartIndex = 0;
        *outEndIndex = paramCount - 1;
        return i;
    }
    if (c == '|') {
        // TODO(memory leak): rangeArgsSeparator is not freed currently!
        *outStartIndex = 0;
        *outEndIndex = paramCount - 1;
        return processAllArgsSpecifier(i, outAllArgsSeparator);
    }

    // NOTE: next it can either be a single index %x or a range %x:y
    // TODO: the index can currently be at max. 10 digits long (make it better)
    int xIndex = -1;
    int newI = matchesInteger(format, formatLength, i, 10, &xIndex);
    if (newI == i) {
        exitWithError("'%' needs to be followed by a positive integer");
    }
    if (xIndex <= 0) {
        exitWithError("Provided range start must be >= 1");
    }
    xIndex--; // User facing is a '1'-based index
    if (xIndex >= paramCount) {
        exitWithError("Selected range start is out of bounds");
    }
    i = newI;
    *outStartIndex = xIndex;

    c = getCharAt(i, "Expected ':' or end of substitution specifier, got end of string");
    if (c == ':') {
        // It's a range: %x:y -> process y part
        i++; // go past ':'
        int yIndex = -1;
        int newI = matchesInteger(format, formatLength, i, 10, &yIndex);
        if (newI == i) {
            exitWithError("':' needs to be followed by a positive integer");
        }
        if (yIndex <= 0) {
            exitWithError("Provided range end must be >= 1");
        }
        yIndex--; // User facing is a '1'-based index
        if (yIndex >= paramCount) {
            exitWithError("Selected range end is out of bounds");
        }
        i = newI;
        *outEndIndex = yIndex;
    } else {
        // It's a single index: %x
        *outEndIndex = xIndex; // Basically the same as writing %x:x
    }

    c = getCharAt(i, "Expected '|' or end of substitution specifier, got end of string");
    if (c == '|') {
        return processAllArgsSpecifier(i, outAllArgsSeparator);
    }
    return i;
}

// takes index of first character inside specifier (after '{')
// returns the new 'i'
int processSpecifier(int i) {
    assert(i >= 0);

    bool isEmptySpecifier = true;
    int startParamIndex = autoIndex;
    int endParamIndex = autoIndex;
    bool indexIsPositional = false;
    char* rangeArgsSeparator = " ";

    char c = getCharAt(i, "Open specifier was not closed; expected '}'");
    while (c != '}') {
        switch(c) {
        case ' ':
        case '\t':
            i++;
            break; // ignore whitespace
        case '#':
            i = processColor(i + 1);
            isEmptySpecifier = false;
            break;
        case '!':
            i = processStyle(i + 1);
            isEmptySpecifier = false;
            break;
        case '%':
            i = processSubstitution(i + 1, &rangeArgsSeparator, &startParamIndex, &endParamIndex);
            if (startParamIndex == endParamIndex) {
                // We need to set these to check that positional and auto-increment
                // indexing is not used together (user can only use either one)
                usedPositionalIndex = true; // -> was a positional index used at least once?
                indexIsPositional = true;   // -> was a positional index used in this specifier?
            }
            isEmptySpecifier = false;
            break;
        default: {
            const char *stylePrefix = "style=";
            const int stylePrefixLen = strlen(stylePrefix);
            if (matchesStringCaseInsensitive(format, formatLength, i, stylePrefix, stylePrefixLen)) {
                i = processStyle(i + stylePrefixLen);
                isEmptySpecifier = false;
            } else {
                exitWithError("Encountered unsupported specifier option");
            }
            break;
        }
        }

        c = getCharAt(i, "Open specifier was not closed; expected '}'");
    }

    assert(startParamIndex <= endParamIndex);
    assert(startParamIndex >= 0);
    assert(endParamIndex < paramCount);

    if (startParamIndex != endParamIndex) {
        for (int i = startParamIndex; i <= endParamIndex; i++) {
            fputs(params[i], stdout);
            if (rangeArgsSeparator && i != endParamIndex) {
                fputs(rangeArgsSeparator, stdout);
            }
        }
    } else {
        if ((usedPositionalIndex && !indexIsPositional)
            || (autoIndex > 0 && indexIsPositional)) {
            exitWithError("Positional indexing ('%xx') and auto-indexing cannot be mixed!");
        }

        if (startParamIndex >= paramCount) {
            exitWithError("Number of specifiers > number of arguments provided; index out of range");
        }

        fputs(params[startParamIndex], stdout);

        if (startParamIndex == autoIndex && !indexIsPositional) {
            autoIndex++;
        }
    }

    if (!isEmptySpecifier) {
        resetStyle();
    }

    return i;
}

int main(int argc, char **argv) {
    if (argc < 2) {
        exitWithError("Please provide the format string");
    }

    format       = argv[1];
    formatLength = strlen(format);
    // Remaining arguments after format
    params       = argv + 2;
    paramCount   = argc - 2;

    // Handle special case of 'zuncito "" "foo" "bar"' -> foobar  
    if (formatLength == 0) {
        for (int i = 0; i < paramCount; i++) {
            fputs(params[i], stdout);
        }
    } else {
        for (int i = 0; i < formatLength; i++) {
            const char c = format[i];
            if (c == '{') {
                i = processSpecifier(i + 1);
            } else if (c == '}') {
                // error, because if we would be inside a specifier, we
                // would be inside processSpecifier and in the following
                // iteration, we are already one character behind the '}'.
                exitWithError("Found '}' before corresponding '{'.");
            } else {
                putc(c, stdout);
            }
        }
    }

    resetStyle();

    return 0;
}
