#include "../header/functions.h"

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <ctype.h>

void exitWithError(const char *msg) {
    resetStyle();
    fflush(stdout);
    fprintf(stderr, "\nError: %s\n", msg);
    exit(1);
}

void resetStyle(void) {
    fputs("\033[0m", stdout);
}

void setStyle(int n) {
    printf("\033[%im", n);
}

static int getFgCode(int code) {
    if (code <= 7) return code + 30;
    // -8 to subtract 'non bright color code range' -> correct offset
    return code - 8 + 90;
}

static int getBgCode(int code) {
    if (code <= 7) return code + 40;
    // -8 to subtract 'non bright color code range' -> correct offset
    return code - 8 + 100;
}

void setForeground(Color color) {
    if (color.type == COLOR_TYPE_CODE) {
        printf("\033[%im", getFgCode(color.data.code));
    } else if (color.type == COLOR_TYPE_RGB) {
        printf("\033[38;2;%i;%i;%im", color.data.rgb[0], color.data.rgb[1], color.data.rgb[2]);
    }
}

void setBackground(Color color) {
    if (color.type == COLOR_TYPE_CODE) {
        printf("\033[%im", getBgCode(color.data.code));
    } else if (color.type == COLOR_TYPE_RGB) {
        printf("\033[48;2;%i;%i;%im", color.data.rgb[0], color.data.rgb[1], color.data.rgb[2]);
    }
}

void setForeAndBackground(Color fg, Color bg) {
    if (fg.type == COLOR_TYPE_CODE && bg.type == COLOR_TYPE_CODE) {
        printf("\033[%i;%im", getFgCode(fg.data.code), getBgCode(bg.data.code));
    } else if (fg.type == COLOR_TYPE_RGB && bg.type == COLOR_TYPE_RGB) {
        printf("\033[38;2;%i;%i;%im", fg.data.rgb[0], fg.data.rgb[1], fg.data.rgb[2]);
        printf("\033[48;2;%i;%i;%im", bg.data.rgb[0], bg.data.rgb[1], bg.data.rgb[2]);
    } else {
        exitWithError("Foreground and background color type does not match!");
    }
}

// parameter i should be the first character thats part of the hex color string
// TODO: Maybe this can just be a call to 'strtol' for the whole RRGGBB string
//       and then extract the individual components out of the integer (may be faster & cleaner?)
bool matchesRGBHexColorString(const char *str, unsigned int strLen, int i, unsigned char *outRed, unsigned char *outGreen, unsigned char *outBlue) {
    assert(str);
    assert(strLen > 0);
    assert(i >= 0);

    char curColor[3] = {0}; // Length of 3 to have a '\0' at the end!
    const int hexStringLen = 2 + 2 + 2; // 2 per color component; 6 in total
    for (int j = 0; i + j < strLen && j < hexStringLen; j++) {
        char c = str[i + j];
        bool isValidChar = 
            (c >= '0' && c <= '9') ||
            (c >= 'a' && c <= 'f') ||
            (c >= 'A' && c <= 'F');
        if (!isValidChar) {
            return false;
        }
        curColor[j % 2] = c;
        if (j == 1 && outRed) {
            *outRed = (unsigned char)strtol(curColor, NULL, 16);
        }
        if (j == 3 && outGreen) {
            *outGreen = (unsigned char)strtol(curColor, NULL, 16);
        }
        if (j == 5 && outBlue) {
            *outBlue = (unsigned char)strtol(curColor, NULL, 16);
        }
    }

    return true;
}

// parameter i should be the first character thats part of the integer
// returns new 'i'. Returns the same i when it could not match an integer!
// TODO: Does currently NOT match negative integer!
int matchesInteger(const char *str, unsigned int strLen, int i, int maxDigits, int *outParsedInteger) {
    assert(str);
    assert(strLen > 0);
    assert(i >= 0);

    if (maxDigits <= 0) return false;

    char *digitBuffer = (char*)malloc(maxDigits + 1);
    if (!digitBuffer) {
        exitWithError("Failed to malloc for digitBuffer");
    }

    int digits;
    for (digits = 0; i + digits < strLen && digits < maxDigits; digits++) {
        char c = str[i + digits];
        if (c < '0' || c > '9') break;
        digitBuffer[digits] = c;
    }
    digitBuffer[digits] = '\0';

    if (outParsedInteger) {
        *outParsedInteger = atoi(digitBuffer);
    }
    free(digitBuffer);

    return i + digits;
}

// parameter i should be the first character that should match searchStr
bool matchesString(const char *str, unsigned int strLen, int i, const char *searchStr, unsigned int searchStrLen) {
    assert(str);
    assert(strLen > 0);
    assert(i >= 0);

    if (!searchStr) return false;
    if (searchStrLen == 0) return false;

    int j = 0;
    for (j = 0; j < searchStrLen && i + j < strLen; j++) {
        if (str[i + j] != searchStr[j]) {
            return false;
        }
    }
    if (j != searchStrLen) {
        return false; // Did only match prefix of searchStr
    }

    return true;
}

// parameter i should be the first character that should match searchStr
bool matchesStringCaseInsensitive(const char *str, unsigned int strLen, int i, const char *searchStr, unsigned int searchStrLen) {
    assert(str);
    assert(strLen > 0);
    assert(i >= 0);

    if (!searchStr) return false;
    if (searchStrLen == 0) return false;

    int j = 0;
    for (j = 0; j < searchStrLen && i + j < strLen; j++) {
        if (tolower(str[i + j]) != tolower(searchStr[j])) {
            return false;
        }
    }
    if (j != searchStrLen) {
        return false; // Did only match prefix of searchStr
    }

    return true;
}
