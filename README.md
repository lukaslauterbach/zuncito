# zuncito
*Status: alpha => expect bugs/inconsitencies/...!*

**Lightweight colorful and styled terminal output**.

The name is derived from one of the lightest bird species which is also colorful: [Bee Hummingbird or zunzuncito](https://en.wikipedia.org/wiki/Bee_hummingbird).

*TODO: Add ascii cinema showcase*

# Specification
`zuncito` takes at least 1 argument: a format and as many arguments as necessary.

It works in the style of `printf`.

## Format
The format is a string of characters made of regular strings and placeholders.

Example: `Some text followed by a specifier {}`

## Specifiers
Specified in `{}`, rust/python style.

A specifier may contain whitespaces (` `, `\t`, ...) for an easier read.

A specifier's text and color parts may be in any order.

The specifier may contain the following information.

### A color
The color is indicated with `#`.

The color may be regular or bright.

The color is specified with different formats: code (number), single letter or word.

The color short names are based on the
RGB: `r`ed, `g`reen, `b`lue,
and CMYKW: `c`yan, `m`agenta, `y`ellow, blac`k` + `w`hite conventions.

Lower case is the normal variant, upper case is the bright variant.

#### Named colors reference table
| Name    | code | short |  long   | code bright | short bright | long bright |
|---------|:----:|:-----:|:-------:|:-----------:|:------------:|:-----------:|
| Black   |  0   |   k   |  black  |      8      |      K       |    BLACK    |
| Red     |  1   |   r   |   red   |      9      |      R       |     RED     |
| Green   |  2   |   g   |  green  |     10      |      G       |    GREEN    |
| Yellow  |  3   |   y   | yellow  |     11      |      Y       |   YELLOW    |
| Blue    |  4   |   b   |  blue   |     12      |      B       |    BLUE     |
| Magenta |  5   |   m   | magenta |     13      |      M       |   MAGENTA   |
| Cyan    |  6   |   c   |  cyan   |     14      |      C       |    CYAN     |
| White   |  7   |   w   |  white  |     15      |      W       |    WHITE    |

You can also use any hexadecimal color like brown: `#54370f`.

The usual notation applies: `#RRGGBB` in hexadecimal format.
The letters may be either lower or upper case.

The color can also be specified with the `rgb()` function.
It takes 3 parameters: red, green and blue ranging between 0 and 255 included.

#### Foreground/background
The color may be applied to either the foreground, the background or both.

Split with slash foreground over background.

`#foreground/background`

Either side is optional.

`#red` is a red font, regular background.
`#/green` is a regular font, green background.
`#red/green` is a red font, green background.
`#/` is no color, same as not specifying a color.

### A style
Supports all styles that ANSI escape codes allow.

A style can be specified with the long option or the short option 
`{style=bold}` or `{!bold}`.

It can also be specified with a single letter `{style=s}` or `{!s}`.

The options for the style specifiers are not case-sensitive.

|  Long name  |               Alternatives               | Short name |
|:-----------:|:----------------------------------------:|:----------:|
|   strong    |                   bold                   |     s      |
|     dim     |                  faint                   |     d      |
|   italic    |                                          |     i      |
|  underline  |               underscored                |     u      |
|    blink    |                 blinking                 |     b      |
|  reversed   | inverse inversed invert inverted reverse |     r      |
|   hidden    |                invisible                 |     h      |
| crossed-out |           strike strikethrough           |     c      |

It's possible to combine the styles.

For instance `zuncito 'foo {!italic!bold!dim!blink!strike!underline#red}' bar` for maximum visibility 🌟.

Further examples:
```
!bold,hidden
!bold !hidden
style=bold,hidden
style=bold style=hidden
!b,h
style=b,h
!B,H
!bold,h
style=strong,i,r
```

### A reference
Unlike `printf` where the order of arguments is forced,
`zuncito` may use arguments in any order with `%x`
where `x` is an integer referring to the arguments passed to `zuncito`.

To use it like `printf`, do

```bash
zuncito '{} {} {}' a b c
```

`a b c`

To select an argument by its position, do

```bash
zuncito '{%3} {%2} {%1}' a b c
```

`c b a`

For clarity, it's not possible to mix index-based and positional arguments.

## Multiple arguments
Use `{%}` to substitude all parameters with a default separator of space: ` `.

Example:
```bash
> zuncito 'hello {%1} {%} {%1}' world foo
`hello world world foo world`
```

Specifying a range is also possible:
```bash
> zuncito 'hello {%1} {%2,3} {%4}' world foo bar bazz
`world foo bar bazz`
```

It's also possible to customize the separator using pipes.

```bash
> zuncito 'hello {%1} {%|, |} {%1}' 1 2 3 4
1 1, 2, 3, 4 1 
```


```bash
> zuncito 'hello {%|\||}' a b
a|b
```

It's possible to give a style to all the arguments at once using the previous specifiers.

`zuncito {% style=bold color=red}`

Will print all the arguments in bold red.

# Alternative rust implementation (same spec/usage)
[cecho](https://github.com/ununhexium/cecho)
