#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <stdio.h>

#include "../header/functions.h"

typedef struct {
    bool success;
    const char *msg;
    unsigned int line;
} TestResult;

typedef TestResult (*TestFunction)(void);

typedef struct {
    const char *name;
    TestFunction fn;
} TestCase;

#define FAIL(msg) return (TestResult){false,(msg),(__LINE__)}
#define ASSERT_EQUALS_INT_CUST(msg, msgSize, expected, result, additional, ...) \
    if (expected != result) { \
    snprintf(msg, msgSize, "expected %s=%i ; got %s=%i " additional, #result, expected, #result, result, __VA_ARGS__); \
    FAIL(msg); }
#define ASSERT_EQUALS_INT(msg, msgSize, expected, result) \
    ASSERT_EQUALS_INT_CUST(msg, msgSize, expected, result, "", NULL)

TestResult test_matchesInteger(void) {
    static char msg[100] = {0}; // static so it outlives this function (ptr is stored in result)
    static int msgSize = sizeof(msg)/sizeof(msg[0]);

    const char *str = "13";
    unsigned int strLen = strlen(str);
    int out = -1;
    bool result = false;

    result = matchesInteger(str, strLen, 0, 0, &out);
    ASSERT_EQUALS_INT(msg, msgSize, false, result);

    result = matchesInteger(str, strLen, 0, 1, &out);
    ASSERT_EQUALS_INT(msg, msgSize, true, result);
    ASSERT_EQUALS_INT(msg, msgSize, 1, out);

    result = matchesInteger(str, strLen, 0, 2, &out);
    ASSERT_EQUALS_INT(msg, msgSize, true, result);
    ASSERT_EQUALS_INT(msg, msgSize, 13, out);

    result = matchesInteger(str, strLen, 0, 10, &out);
    ASSERT_EQUALS_INT(msg, msgSize, true, result);
    ASSERT_EQUALS_INT(msg, msgSize, 13, out);

    const char *invalidStr = "eisvogel";
    unsigned int invalidStrLen = strlen(invalidStr);

    for (int maxDigits = 0; maxDigits < invalidStrLen + 1; maxDigits++) {
        result = matchesInteger(invalidStr, invalidStrLen, 0, maxDigits, &out);
        ASSERT_EQUALS_INT_CUST(msg, msgSize, false, result, "for maxDigits=%i", maxDigits);
    }

    return (TestResult){ true, 0, 0 };
}

int main(int argc, char **argv) {
    TestCase testCases[] = {
        {
            "matchesInteger",
            &test_matchesInteger
        }
    };

    bool failed = false;
    for (int i = 0; i < sizeof(testCases) / sizeof(testCases[0]); i++) {
        printf("Running %s...", testCases[i].name);
        TestResult result = testCases[i].fn();
        if (result.success) {
            Color color;
            color.type = COLOR_TYPE_CODE;
            color.data.code = 2;
            setForeground(color);
            printf("Ok!\n");
            resetStyle();
        } else {
            Color color;
            color.type = COLOR_TYPE_CODE;
            color.data.code = 1;
            setForeground(color);
            printf("Failed");
            resetStyle();
            printf(" at line %u:\n%s\n", result.line, result.msg);
            failed = true;
        }
    }

    return failed;
}
