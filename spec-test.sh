#!/bin/sh
# Usage: ./spec-test.sh path-to-exe
# Example: ./spec-test.sh ./zuncito
# Info: testcases are at the bottom of this file

EXE="${1}"
if [ "${EXE}" = "" ]; then
    echo "Error: Provide path to exe"
    exit 1
fi

ESC="\033"

BLACK="${ESC}[30m"
RED="${ESC}[31m"
GREEN="${ESC}[32m"
YELLOW="${ESC}[33m"
BLUE="${ESC}[34m"
WHITE="${ESC}[37m"
BG_BLACK="${ESC}[40m"
BG_RED="${ESC}[41m"
BG_GREEN="${ESC}[42m"
BG_YELLOW="${ESC}[43m"
BG_BLUE="${ESC}[44m"
BG_WHITE="${ESC}[47m"

BRIGHT_BLACK="${ESC}[90m"
BRIGHT_RED="${ESC}[91m"
BRIGHT_YELLOW="${ESC}[93m"
BG_BRIGHT_BLACK="${ESC}[100m"
BG_BRIGHT_RED="${ESC}[101m"
BG_BRIGHT_YELLOW="${ESC}[103m"

BOLD="${ESC}[1m"
UNDERLINE="${ESC}[4m"
RESET="${ESC}[0m"

PASSED_TESTS=0
FAILED_TESTS=0

# This function executes a testcase
# $1 is test group
# $2 is test case name/description
# $3 is expected result
#    - a reset style ('${ESC}[0m') is automatically appended to the passed expected result
# $4..n are exe arguments
testcase() {
    GROUP="${1}"
    NAME="${2}"
    EXPECTED="${3}${RESET}"
    shift; shift; shift # such that $@ is $4..n

    RESULT=$("${EXE}" "${@}")
    RESULT_EXIT_CODE=${?}

    EXPECTED_HEX=$(printf "${EXPECTED}" | hexdump)
    RESULT_HEX=$(printf "${RESULT}" | hexdump)

    PREFIX="${BRIGHT_BLACK}[${GROUP}]${RESET} ${NAME}: "

    if [ "${RESULT_HEX}" = "${EXPECTED_HEX}" -a ${RESULT_EXIT_CODE} -eq 0 ]; then
        printf "${PREFIX}${BOLD}${GREEN}Ok!${RESET}\n"
        PASSED_TESTS=$((${PASSED_TESTS}+1))
    else
        if [ ${RESULT_EXIT_CODE} -eq 0 ]; then
            printf "${PREFIX}${BOLD}${RED}Failed!${RESET}\n"
        else
            printf "${PREFIX}${BOLD}${RED}Failed with exit-code ${RESULT_EXIT_CODE}!${RESET}\n"
        fi
        printf "\tCommand: %s\n" "${EXE} ${*}"
        printf "\t${BOLD}Expected:${RESET}\n%s\n" "$(echo ${EXPECTED} | hexdump -C)"
        printf "\t${BOLD}Actual:${RESET}\n%s\n" "$(echo ${RESULT}   | hexdump -C)"
        FAILED_TESTS=$((${FAILED_TESTS}+1))
    fi
}

# ------- Test Cases ------- #
# TODO: Add tests with "weird" whitespace where allowed
# General
testcase "general" "Empty format string prints all arguments" "foobarbazz" '' foo bar bazz

# Substitution
testcase "substitution" "Auto increment substitution" "foo + bar = bazz" '{} + {} = {}' foo bar bazz
testcase "substitution" "Indexed" "bazz${RESET} - bar${RESET} = foo${RESET}" '{%3} - {%2} = {%1}' foo bar bazz
testcase "substitution" "Indexed with empty separator" "bazz${RESET}" '{%3||}' foo bar bazz fuzz
testcase "substitution" "Indexed with custom separator" "bazz${RESET}" '{%3|, |}' foo bar bazz fuzz
testcase "substitution" "Indexed with escaped separator" "bazz${RESET}" '{%3|\||}' foo bar bazz fuzz

testcase "substitution" "All with standard separator" "foo bar bazz${RESET}" '{%}' foo bar bazz
testcase "substitution" "All with empty separator" "foobarbazz${RESET}" '{%||}' foo bar bazz
testcase "substitution" "All with custom separator" "foo, bar, bazz${RESET}" '{%|, |}' foo bar bazz
testcase "substitution" "All with escaped separator" "foo|bar|bazz${RESET}" '{%|\||}' foo bar bazz

testcase "substitution" "Range with standard separator" "bar bazz${RESET}" '{%2:3}' foo bar bazz fuzz
testcase "substitution" "Range with empty separator" "barbazz${RESET}" '{%2:3||}' foo bar bazz fuzz
testcase "substitution" "Range with custom separator" "bar, bazz${RESET}" '{%2:3|, |}' foo bar bazz fuzz
testcase "substitution" "Range with escaped separator" "bar|bazz${RESET}" '{%2:3|\||}' foo bar bazz fuzz

# Color
# TODO: Add tests with mixed color specifiers (e.g. code and rgb)
testcase "color" "foreground code" "${RED}foo${RESET}" '{#1}' foo
testcase "color" "foreground short" "${YELLOW}foo${RESET}" '{#y}' foo
testcase "color" "foreground long" "${BLACK}foo${RESET}" '{#black}' foo
testcase "color" "foreground bright code" "${BRIGHT_RED}foo${RESET}" '{#9}' foo
testcase "color" "foreground bright short" "${BRIGHT_YELLOW}foo${RESET}" '{#Y}' foo
testcase "color" "foreground bright long" "${BRIGHT_BLACK}foo${RESET}" '{#BLACK}' foo
testcase "color" "foreground hex" "${ESC}[38;2;255;0;0mfoo${RESET}" '{#FF0000}' foo
testcase "color" "foreground rgb" "${ESC}[38;2;200;100;50mfoo${RESET}" '{#rgb(200,100,50)}' foo

testcase "color" "background code" "${BG_RED}foo${RESET}" '{#/1}' foo
testcase "color" "background short" "${BG_YELLOW}foo${RESET}" '{#/y}' foo
testcase "color" "background long" "${BG_BLACK}foo${RESET}" '{#/black}' foo
testcase "color" "background bright code" "${BG_BRIGHT_RED}foo${RESET}" '{#/9}' foo
testcase "color" "background bright short" "${BG_BRIGHT_YELLOW}foo${RESET}" '{#/Y}' foo
testcase "color" "background bright long" "${BG_BRIGHT_BLACK}foo${RESET}" '{#/BLACK}' foo
testcase "color" "background hex" "${ESC}[48;2;255;0;0mfoo${RESET}" '{#/FF0000}' foo
testcase "color" "background rgb" "${ESC}[48;2;200;100;50mfoo${RESET}" '{#/rgb(200,100,50)}' foo

testcase "color" "combined code" "${ESC}[31;42mfoo${RESET}" '{#1/2}' foo
testcase "color" "combined short" "${ESC}[33;40mfoo${RESET}" '{#y/k}' foo
testcase "color" "combined long" "${ESC}[34;45mfoo${RESET}" '{#blue/magenta}' foo
testcase "color" "combined bright code" "${ESC}[91;102mfoo${RESET}" '{#9/10}' foo
testcase "color" "combined bright short" "${ESC}[93;107mfoo${RESET}" '{#Y/W}' foo
testcase "color" "combined bright long" "${ESC}[95;106mfoo${RESET}" '{#MAGENTA/CYAN}' foo
testcase "color" "combined hex" "${ESC}[38;2;255;0;0m${ESC}[48;2;0;255;0mfoo${RESET}" '{#FF0000/00FF00}' foo
testcase "color" "combined rgb" "${ESC}[38;2;200;100;50m${ESC}[48;2;128;75;255mfoo${RESET}" '{#rgb(200,100,50)/rgb(128,75,255)}' foo

# Style (!xxx)
STRONG_RESULT="${ESC}[1mfoo${RESET}"
testcase "style (!xxx)" "strong short name" "${STRONG_RESULT}" '{!s}' foo
testcase "style (!xxx)" "strong long name" "${STRONG_RESULT}" '{!strong}' foo
testcase "style (!xxx)" "strong long name (alternative 'bold')" "${STRONG_RESULT}" '{!bold}' foo

DIM_RESULT="${ESC}[2mfoo${RESET}"
testcase "style (!xxx)" "dim short name" "${DIM_RESULT}" '{!d}' foo
testcase "style (!xxx)" "dim long name" "${DIM_RESULT}" '{!dim}' foo
testcase "style (!xxx)" "dim long name (alternative 'faint')" "${DIM_RESULT}" '{!faint}' foo

ITALIC_RESULT="${ESC}[3mfoo${RESET}"
testcase "style (!xxx)" "italic short name" "${ITALIC_RESULT}" '{!i}' foo
testcase "style (!xxx)" "italic long name" "${ITALIC_RESULT}" '{!italic}' foo

UNDERLINE_RESULT="${ESC}[4mfoo${RESET}"
testcase "style (!xxx)" "underline short name" "${UNDERLINE_RESULT}" '{!u}' foo
testcase "style (!xxx)" "underline long name" "${UNDERLINE_RESULT}" '{!underline}' foo
testcase "style (!xxx)" "underline long name (alternative 'underscored')" "${UNDERLINE_RESULT}" '{!underscored}' foo

BLINK_RESULT="${ESC}[5mfoo${RESET}"
testcase "style (!xxx)" "blink short name" "${BLINK_RESULT}" '{!b}' foo
testcase "style (!xxx)" "blink long name" "${BLINK_RESULT}" '{!blink}' foo
testcase "style (!xxx)" "blink long name (alternative 'blinking')" "${BLINK_RESULT}" '{!blinking}' foo

REVERSED_RESULT="${ESC}[7mfoo${RESET}"
testcase "style (!xxx)" "reversed short name" "${REVERSED_RESULT}" '{!r}' foo
testcase "style (!xxx)" "reversed long name" "${REVERSED_RESULT}" '{!reversed}' foo
testcase "style (!xxx)" "reversed long name (alternative 'inverse')" "${REVERSED_RESULT}" '{!inverse}' foo
testcase "style (!xxx)" "reversed long name (alternative 'inversed')" "${REVERSED_RESULT}" '{!inversed}' foo
testcase "style (!xxx)" "reversed long name (alternative 'invert')" "${REVERSED_RESULT}" '{!invert}' foo
testcase "style (!xxx)" "reversed long name (alternative 'inverted')" "${REVERSED_RESULT}" '{!inverted}' foo
testcase "style (!xxx)" "reversed long name (alternative 'reverse')" "${REVERSED_RESULT}" '{!reverse}' foo

HIDDEN_RESULT="${ESC}[8mfoo${RESET}"
testcase "style (!xxx)" "hidden short name" "${HIDDEN_RESULT}" '{!h}' foo
testcase "style (!xxx)" "hidden long name" "${HIDDEN_RESULT}" '{!hidden}' foo
testcase "style (!xxx)" "hidden long name (alternative 'invisible')" "${HIDDEN_RESULT}" '{!invisible}' foo

CROSSED_OUT_RESULT="${ESC}[9mfoo${RESET}"
testcase "style (!xxx)" "crossed-out short name" "${CROSSED_OUT_RESULT}" '{!c}' foo
testcase "style (!xxx)" "crossed-out long name" "${CROSSED_OUT_RESULT}" '{!crossed-out}' foo
testcase "style (!xxx)" "crossed-out long name (alternative 'strike')" "${CROSSED_OUT_RESULT}" '{!strike}' foo
testcase "style (!xxx)" "crossed-out long name (alternative 'strikethrough')" "${CROSSED_OUT_RESULT}" '{!strikethrough}' foo

# Style (style=xxx)
STRONG_RESULT="${ESC}[1mfoo${RESET}"
testcase "style=xxx" "strong short name" "${STRONG_RESULT}" '{style=s}' foo
testcase "style=xxx" "strong long name" "${STRONG_RESULT}" '{style=strong}' foo
testcase "style=xxx" "strong long name (alternative 'bold')" "${STRONG_RESULT}" '{style=bold}' foo

DIM_RESULT="${ESC}[2mfoo${RESET}"
testcase "style=xxx" "dim short name" "${DIM_RESULT}" '{style=d}' foo
testcase "style=xxx" "dim long name" "${DIM_RESULT}" '{style=dim}' foo
testcase "style=xxx" "dim long name (alternative 'faint')" "${DIM_RESULT}" '{style=faint}' foo

ITALIC_RESULT="${ESC}[3mfoo${RESET}"
testcase "style=xxx" "italic short name" "${ITALIC_RESULT}" '{style=i}' foo
testcase "style=xxx" "italic long name" "${ITALIC_RESULT}" '{style=italic}' foo

UNDERLINE_RESULT="${ESC}[4mfoo${RESET}"
testcase "style=xxx" "underline short name" "${UNDERLINE_RESULT}" '{style=u}' foo
testcase "style=xxx" "underline long name" "${UNDERLINE_RESULT}" '{style=underline}' foo
testcase "style=xxx" "underline long name (alternative 'underscored')" "${UNDERLINE_RESULT}" '{style=underscored}' foo

BLINK_RESULT="${ESC}[5mfoo${RESET}"
testcase "style=xxx" "blink short name" "${BLINK_RESULT}" '{style=b}' foo
testcase "style=xxx" "blink long name" "${BLINK_RESULT}" '{style=blink}' foo
testcase "style=xxx" "blink long name (alternative 'blinking')" "${BLINK_RESULT}" '{style=blinking}' foo

REVERSED_RESULT="${ESC}[7mfoo${RESET}"
testcase "style=xxx" "reversed short name" "${REVERSED_RESULT}" '{style=r}' foo
testcase "style=xxx" "reversed long name" "${REVERSED_RESULT}" '{style=reversed}' foo
testcase "style=xxx" "reversed long name (alternative 'inverse')" "${REVERSED_RESULT}" '{style=inverse}' foo
testcase "style=xxx" "reversed long name (alternative 'inversed')" "${REVERSED_RESULT}" '{style=inversed}' foo
testcase "style=xxx" "reversed long name (alternative 'invert')" "${REVERSED_RESULT}" '{style=invert}' foo
testcase "style=xxx" "reversed long name (alternative 'inverted')" "${REVERSED_RESULT}" '{style=inverted}' foo
testcase "style=xxx" "reversed long name (alternative 'reverse')" "${REVERSED_RESULT}" '{style=reverse}' foo

HIDDEN_RESULT="${ESC}[8mfoo${RESET}"
testcase "style=xxx" "hidden short name" "${HIDDEN_RESULT}" '{style=h}' foo
testcase "style=xxx" "hidden long name" "${HIDDEN_RESULT}" '{style=hidden}' foo
testcase "style=xxx" "hidden long name (alternative 'invisible')" "${HIDDEN_RESULT}" '{style=invisible}' foo

CROSSED_OUT_RESULT="${ESC}[9mfoo${RESET}"
testcase "style=xxx" "crossed-out short name" "${CROSSED_OUT_RESULT}" '{style=c}' foo
testcase "style=xxx" "crossed-out long name" "${CROSSED_OUT_RESULT}" '{style=crossed-out}' foo
testcase "style=xxx" "crossed-out long name (alternative 'strike')" "${CROSSED_OUT_RESULT}" '{style=strike}' foo
testcase "style=xxx" "crossed-out long name (alternative 'strikethrough')" "${CROSSED_OUT_RESULT}" '{style=strikethrough}' foo

# Style (Multiple styles)
testcase "style (!xx,yy,zz)" "multiple styles short" "${ESC}[5m${ESC}[4mfoo${RESET}" '{!b,u}' foo
testcase "style (!xx,yy,zz)" "multiple styles long" "${ESC}[5m${ESC}[4mfoo${RESET}" '{!blink,underline}' foo
testcase "style (!xx,yy,zz)" "multiple styles mixed 1" "${ESC}[5m${ESC}[4mfoo${RESET}" '{!b,underline}' foo
testcase "style (!xx,yy,zz)" "multiple styles mixed 2" "${ESC}[5m${ESC}[4mfoo${RESET}" '{!blink,u}' foo

testcase "style=xx,yy,zz" "multiple styles short" "${ESC}[5m${ESC}[4mfoo${RESET}" '{style=b,u}' foo
testcase "style=xx,yy,zz" "multiple styles long" "${ESC}[5m${ESC}[4mfoo${RESET}" '{style=blink,underline}' foo
testcase "style=xx,yy,zz" "multiple styles mixed 1" "${ESC}[5m${ESC}[4mfoo${RESET}" '{style=b,underline}' foo
testcase "style=xx,yy,zz" "multiple styles mixed 2" "${ESC}[5m${ESC}[4mfoo${RESET}" '{style=blink,u}' foo

# Style (Case insensitivity)
STRONG_RESULT="${ESC}[1mfoo${RESET}"
testcase "case-insensitive style (!xxx)" "strong short name" "${STRONG_RESULT}" '{!S}' foo
testcase "case-insensitive style (!xxx)" "strong long name" "${STRONG_RESULT}" '{!STRONG}' foo
testcase "case-insensitive style (!xxx)" "strong long name (alternative 'bold')" "${STRONG_RESULT}" '{!BOLD}' foo

testcase "case-insensitive style=xxx" "strong short name" "${STRONG_RESULT}" '{style=S}' foo
testcase "case-insensitive style=xxx" "strong long name" "${STRONG_RESULT}" '{style=STRONG}' foo
testcase "case-insensitive style=xxx" "strong long name (alternative 'bold')" "${STRONG_RESULT}" '{style=BOLD}' foo

testcase "case-insensitive style (!xx,yy,zz)" "multiple styles mixed 1" "${ESC}[5m${ESC}[4mfoo${RESET}" '{!B,UNDERLINE}' foo
testcase "case-insensitive style (!xx,yy,zz)" "multiple styles mixed 2" "${ESC}[5m${ESC}[4mfoo${RESET}" '{!BLINK,U}' foo
testcase "case-insensitive style=xx,yy,zz" "multiple styles mixed 1" "${ESC}[5m${ESC}[4mfoo${RESET}" '{style=B,UNDERLINE}' foo
testcase "case-insensitive style=xx,yy,zz" "multiple styles mixed 2" "${ESC}[5m${ESC}[4mfoo${RESET}" '{style=BLINK,U}' foo

# Various Specifier Rules
testcase "specifiers" "Whitespace inside specifiers is ignored" "foo bar bazz" '{   } {	 } {}' foo bar bazz # Careful the second {} has a literal tab character in it


# ------- Statistics ------- #
printf "\n${BOLD}Result:${RESET}\n\tFailed: ${RED}${FAILED_TESTS}${RESET}\n\tPassed: ${GREEN}${PASSED_TESTS}${RESET}\n"

if [ ${FAILED_TESTS} -eq 0 ]; then
    exit 0
else
    exit 1
fi
