CC_FLAGS=-Wall -pedantic

release: header/*.h src/*.c
	${CC} ${CC_FLAGS} -O3 src/*.c zuncito.c -o zuncito

debug: header/*.h src/*.c
	${CC} ${CC_FLAGS} -O0 -g src/*.c zuncito.c -o zuncito

test: unit-test e2e-test

unit-test: header/*.h test/*.c
	@${CC} ${CC_FLAGS} test/*.c src/*.c -o tests
	@./tests
	@rm -f tests

e2e-test: release
	@./spec-test.sh ./zuncito

clean:
	rm -f zuncito

.PHONY: test clean
